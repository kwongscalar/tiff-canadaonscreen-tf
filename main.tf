#Define variables

variable "environment" {
  description = "The environment - dev, uat, prod, etc."
  default     = "production"
}

variable "region" {
  description = "AWS region"
  default     = "ca-central-1"
}

variable "client_name" {
  description = "The name of the client.  Used for tagging and namespacing."
  default     = "tiff-cos"
}

variable "vpc_cidr" {
  description = "The CIDR of the VPC"
  default     = "10.150.13.0/24"
}

variable "vpc_id" {
  description = "The ID of the VPC"
  default     = "vpc-c610dbaf"
}

variable "public_vpc_subnet_range_az1_id" {
  description = "The public subnet range for AZ1"
  default     = "subnet-f78e5f9e"
}

variable "public_vpc_subnet_range_az2_id" {
  description = "The public subnet range for AZ2"
  default     = "subnet-6eef1b15"
}

variable "private_vpc_subnet_range_app_az1_id" {
  description = "The public subnet range for AZ1"
  default     = "subnet-a98e5fc0"
}

variable "private_vpc_subnet_range_app_az2_id" {
  description = "The public subnet range for AZ2"
  default     = "subnet-43ef1b38"
}

variable "ips_allowed_to_elb" {
  description = "The external NAT(s) allowed to the ELB"
  default     = "74.213.172.118/32"
}

variable "key_name" {
  description = "The keypair to use"
  default     = "tiff-cacaentral"
}

variable "instance_type" {
  description = "The instance type for production"
  default     = "t2.large"
}

variable "instance_profile_name" {
  description = "The instance type for production"
  default     = "TiffNet-ca-webserver"
}

variable "instance_port" {
  description = "The port on the instance to route to"
  default     = "80"
}

variable "instance_protocol" {
  description = "The protocol to use for the port.  HTTP, HTTPS, TCP or SSL only."
  default     = "http"
}

variable "lb_port" {
  description = "The port to listen on for the load balancer"
  default     = "80"
}

variable "lb_protocol" {
  description = "The protocol to listen on.  HTTP, HTTPS, TCP or SSL only."
  default     = "http"
}

variable "health_check_healthy_threshold" {
  description = "The number of checks before the instance is declared healthy."
  default     = "5"
}

variable "health_check_unhealthy_threshold" {
  description = "The number of failed checks before the instance is declared unhealthy."
  default     = "3"
}

variable "health_check_target" {
  description = "The actual health check. eg. HTTP:80/test"
  default     = "TCP:22"
}

variable "health_check_interval" {
  description = "The interval between checks."
  default     = "10"
}

variable "health_check_timeout" {
  description = "The timeout before the check fails."
  default     = "2"
}

variable "asg_max_size" {
  description = "The maximum size of the auto scale group."
  default     = "8"
}

variable "asg_min_size" {
  description = "The minimum size of the auto scale group."
  default     = "2"
}

variable "asg_health_check_grace_period" {
  description = "Time in seconds, after instance is in service, before starting to check health."
  default     = "300"
}

variable "asg_health_check_type" {
  description = "The type of health check.  Only ELB or EC2 are valid."
  default     = "ELB"
}

variable "asg_desired_capacity" {
  description = "The number of EC2 instances that should be running in the autoscaling group"
  default     = "2"
}

variable "ami_filter_value" {
  description = "The regex to find the AMI"
  default     = "amzn-ami-hvm-*-x86_64-gp2"
}

variable "ami_owner" {
  default     = "137112412989"
  description = "The owner ID of the AMI."
}

variable "tiffweb_bastion_sg" {
  default     = "sg-3f08a556"
  description = "The existing bastion SG"
}

variable "bootstrap_s3_bucket" {
  default = "tiffweb-ansible"
}

variable "bootstrap_s3_branch" {
  default = "prod"
}

variable "bootstrap_s3_commit_hash" {
  default = "ce6418110b5f78ff2ca328feac7aa63be52d641a"
}


provider "aws" {
  region = "${var.region}"
}

# Create SG for web servers
module "web_secgroup" {
  source      = "./modules/web_secgroup"
  client_name = "${var.client_name}"
  vpc_id      = "${var.vpc_id}"
  vpc_cidr    = "${var.vpc_cidr}"  
  environment = "${var.environment}"
}

# Create SG for Elastic Load Balancers (ELB)
module "elb_secgroup" {
  source             = "./modules/elb_secgroup"
  client_name        = "${var.client_name}"
  vpc_id             = "${var.vpc_id}"
  environment        = "${var.environment}"
  ips_allowed_to_elb = "${var.ips_allowed_to_elb}"
}

# Create S3 Buckets to redirect from root domain to www.<root_domain>
module "s3_buckets_tiffcanadaonscreen" {
  source    = "./modules/s3_buckets"
  s3_bucket = "tiffcanadaonscreen.com"
  region    = "${var.region}"
}

module "s3_buckets_tiffcanadaalecran" {
  source    = "./modules/s3_buckets"
  s3_bucket = "tiffcanadaalecran.com"
  region    = "${var.region}"
}

# Render user_data file
data "template_file" "tiffcanonscreen_user_data" {
  template = "${file("user_data.sh")}"

  vars {
    bootstrap_s3_bucket      = "${var.bootstrap_s3_bucket}"
    bootstrap_s3_branch      = "${var.bootstrap_s3_branch}"
    bootstrap_s3_commit_hash = "${var.bootstrap_s3_commit_hash}"
    aws_region               = "${var.region}"
  }
}

# Create the join-web servers with an autoscaling group
module "tiff-canonscreen-web" {
  source                           = "./modules/elb_asg_external"
  client_name                      = "${var.client_name}"
  vpc_cidr                         = "${var.vpc_cidr}"
  elb_subnets                      = "${var.public_vpc_subnet_range_az1_id},${var.public_vpc_subnet_range_az2_id}"
  asg_subnets                      = "${var.private_vpc_subnet_range_app_az1_id},${var.private_vpc_subnet_range_app_az2_id}"
  environment                      = "${var.environment}"
  server_role                      = "tiff-cos"
  instance_security_groups         = "${module.web_secgroup.web_security_group_id},${var.tiffweb_bastion_sg}"
  elb_security_groups              = "${module.elb_secgroup.elb_security_group_id}"
  instance_type                    = "${var.instance_type}"
  iam_instance_profile             = "${var.instance_profile_name}"
  key_name                         = "${var.key_name}"
  lb_protocol                      = "${var.lb_protocol}"
  lb_port                          = "${var.lb_port}"
  instance_protocol                = "${var.instance_protocol}"
  instance_port                    = "${var.instance_port}"
  health_check_target              = "${var.health_check_target}"
  health_check_timeout             = "${var.health_check_timeout}"
  health_check_interval            = "${var.health_check_interval}"
  health_check_healthy_threshold   = "${var.health_check_healthy_threshold}"
  health_check_unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
  asg_max_size                     = "${var.asg_max_size}"
  asg_min_size                     = "${var.asg_min_size}"
  asg_desired_capacity             = "${var.asg_desired_capacity}"
  asg_health_check_grace_period    = "${var.asg_health_check_grace_period}"
  ami_filter_value                 = "${var.ami_filter_value}"
  ami_owner                        = "${var.ami_owner}"
  user_data_script                 = "${data.template_file.tiffcanonscreen_user_data.rendered}"
}
