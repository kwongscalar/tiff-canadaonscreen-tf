# TIFF Canada On Screen Website

This is the Terraform and Ansible to build the servers required for the following domains:  

```
* tiffcanadaonscreen.com 
* tiffcanadaalecran.com
```

The servers are built with Terraform and consist of regular Apache2 with PHP-FPM on Amazon Linux.

## Troubleshooting

There are only two main processes which would cause an issue.  SSH to each of the servers and try the following commands:

### Restart Apache2

```
sudo service httpd <restart|reload>
```

### Restart PHP-FPM

```
sudo service php-fpm <restart|reload>
```

## Code Deployment

Code deploy is pretty simplistic at this point.  The steps are as follows:

1. 3rd party developer sends us (via email) a zip file of the new build
2. We take that zip file and upload to the S3 bucket canada-on-screen-code
3. SSH to each of the servers and run the code deploy script as root

```
sudo su - 
./deployCOS.sh
```

## Architecture

![alt text](https://bytebucket.org/kwongscalar/tiff-canadaonscreen-tf/raw/7ade546caa42ca9a034aeee970634f27111001c2/documentation/TIFF-CanadaOnScreen-Website-Architecture.png "TIFF-COS-Architecture")

### Servers
There are two servers sitting in an auto-scaling group which just scale up based on CPU.

### Storage
Three S3 buckets are in use for this project.
```
* canada-on-screen-code  (used for code deployments)
* tiffcaandaonscreen.com (used only for redirect to www.tiffcanadaonscreen.com)
* tiffcaandaalecran.com  (used only for redirect to www.tiffcanadaalecran.com)
```

### Networking
The servers for these websites sit within the same VPC as the tiff.net website.  Outbound access for the servers uses the managed NAT gateways.

There were both pros and cons for doing this, but to distill it down, there are links from the main website to these two Canada On Screen websites so they are technically "one app", even though there are no shared components.  **_We need to discuss this and come up for our own "rules of engagement"._**


### Content Distribution Network (CDN)
There are 4 Cloudfront distributions created, of which only two are currently in use.  

The two distributions which are in use are for the S3 redirects to www.<site>.com.  The other two distributions are for the main website, but DNS has not been re-pointed until we get approval from Tiff.

### DNS

The zones for the two domains above were imported into Route53 (in the TIFF AWS account) and the registrar has been modified.  Please make use of ALIAS records if possible - eg. if switching to Cloudfront for the main sites.

