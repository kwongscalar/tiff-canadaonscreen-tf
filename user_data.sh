#!/bin/bash

sleep 10
pip install ansible
mkdir /provisioning
aws s3 cp s3://${bootstrap_s3_bucket}/${bootstrap_s3_branch}/${bootstrap_s3_commit_hash}.tar.bz2 /provisioning/provisioning.tar.bz2 --region=${aws_region}
cd /provisioning
tar xfj provisioning.tar.bz2
mkdir group_vars
aws s3 cp s3://${bootstrap_s3_bucket}/${bootstrap_s3_branch}/all /provisioning/group_vars/all --region=${aws_region}
/usr/local/bin/ansible-playbook -i localhost, -c local canadaonscreen.yml