/* This file is for variable overrides.  
Variables here will override all variables in main.tf
*/

# These vars for testing in Scalar AWS account
# region                              = "us-east-1"
# instance_type                       = "t2.small"
# key_name                            = "kenwong-kp"
# health_check_target                 = "TCP:22"
# vpc_cidr                            = "10.201.28.0/24"
# vpc_id                              = "vpc-673c3d01"
# public_vpc_subnet_range_az1_id      = "subnet-74bc5858"
# public_vpc_subnet_range_az2_id      = "subnet-3f5a8d77"
# private_vpc_subnet_range_app_az1_id = "subnet-d7b054fb"
# private_vpc_subnet_range_app_az2_id = "subnet-8d5b8cc5"
# instance_profile_name               = "ken-ecstest"
# tiffweb_bastion_sg                  = "sg-510c2a2e"

# # Amazon Linux in us-east-1
#ami_filter_value       = "amzn-ami-hvm-*x86_64-gp2"
#ami_owner              = "137112412989"

# # Redhat Linux in us-east-1
# ami_filter_value = "RHEL-7.3_HVM_GA-*-x86_64-1-Hourly2-GP2"
# ami_owner = "309956199498"