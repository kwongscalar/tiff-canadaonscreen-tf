variable "vpc_id" {
  description = "The VPC ID"
}

variable "vpc_cidr" {
  description = "The VPC CIDR"
}

variable "environment" {
  description = "Environment name"
}

variable "client_name" {
  description = "Client name"
}

/* Security group for web instances */
resource "aws_security_group" "client_name_web_security_group" {
  name        = "${var.client_name}-${var.environment}-web-security-group"
  description = "Allow port 80 and 443 from public internet"
  vpc_id      = "${var.vpc_id}"
  
  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.client_name}-${var.environment}-web-security-group"
    Terraform  = "true"
  }
}

