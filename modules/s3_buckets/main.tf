variable "s3_bucket" {}
variable "region" {}

resource "aws_s3_bucket" "bucket" {
	bucket                     = "${var.s3_bucket}"
	acl                        = "public-read"
	region                     = "${var.region}"
	website {
	  redirect_all_requests_to = "www.${var.s3_bucket}"
	}
}