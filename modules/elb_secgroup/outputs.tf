output "elb_security_group_id" {
  value = "${aws_security_group.client_name_elb_security_group.id}"
}

output "elb_security_group_name" {
  value = "${aws_security_group.client_name_elb_security_group.name}"
}

