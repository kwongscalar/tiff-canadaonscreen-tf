# Create a new application load balancer

variable "client_name"  {}
variable "environment"  {}
variable "internal" {}
variable "deletion_protection" {}
variable "access_logs_prefix"  {}
variable "public_vpc_subnet_range_az1_id" {}
variable "public_vpc_subnet_range_az2_id" {}
variable "instance_security_groups" {}
variable "elb_security_groups" {}


# ----------
# Create ALB
# ----------
resource "aws_alb" "client_name_alb" {
  name            = "${var.client_name}-${var.environment}-alb"
  internal        = "${var.internal}" 
  subnets         = ["${split(",", var.elb_subnets)}"]
  security_groups = ["${split(",", var.elb_security_groups)}"]

  enable_deletion_protection = "${var.deletion_protection}"

#  Uncomment to enable access logs
#  access_logs {
#    bucket = "${aws_s3_bucket.alb_logs.bucket}"
#    prefix = "${var.access_logs_prefix}"
#  }

  tags {
    Name        = "${var.client_name}-${var.environment}-alb"
    Environment = "${var.environment}"
  }
}

# -----------------------
# Create ALB Target Group
# -----------------------
resource "aws_alb_target_group" "client_name_alb80_tg" {
  name          = "${var.client_name}-${var.environment}-alb80-tg"
  port          = "80"
  protocol      = "HTTP"
  vpc_id        = "${var.vpc_id}"

  tags {
    Name        = "${var.client_name}-${var.environment}-alb80-tg"
    Environment = "${var.environment}"
  }
}

# ----------------
# Create listeners
# ----------------
resource "aws_alb_listener" "web_http" {
  load_balancer_arn  = "${aws_alb.client_name_alb.arn}"
  port               = "80"
  protocol           = "HTTP"
  default_action {
    target_group_arn = "${aws_alb_target_group.client_name_alb80_tg.arn}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "web_https" {
  load_balancer_arn  = "${aws_alb.client_name_alb.arn}"
  port               = "443"
  protocol           = "HTTPS"
  default_action {
    target_group_arn = "${aws_alb_target_group.client_name_alb80_tg.arn}"
    type             = "forward"
  }
}

# Get arn of scalar certificate in Amazon Certificate Manager
data "aws_acm_certificate" "scalarkitchen" {
  domain = "scalar.kitchen"
  statuses = ["ISSUED"]
}

resource "aws_alb_listener" "web_https" {
  load_balancer_arn  = "${aws_alb.client_name_alb.arn}"
  port               = "443"
  protocol           = "HTTPS"
  ssl_policy         = "ELBSecurityPolicy-2015-05"
  certificate_arn    = "${data.aws_acm_certificate.scalarkitchen.arn}"
  default_action {
    target_group_arn = "${aws_alb_target_group.client_name_alb80_tg.arn}"
    type             = "forward"
  }
}

# ----------------------
# Create listener rules
# ----------------------
resource "aws_alb_listener_rule" "web_http" {
  listener_arn = "${aws_alb_listener.web_http.arn}"
  priority = 100

  action {
    type = "forward"
    target_group_arn = "${aws_alb_target_group.client_name_alb80_tg.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["/scalar80/*"]
  }
}

resource "aws_alb_listener_rule" "web_http9000" {
  listener_arn = "${aws_alb_listener.web_http9000.arn}"
  priority = 100

  action {
    type = "forward"
    target_group_arn = "${aws_alb_target_group.client_name_alb9000_tg.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["/scalar9000/*"]
  }
}
