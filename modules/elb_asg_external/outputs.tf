output "elb_dns_name" {
  value = "${aws_elb.client_name_elb.dns_name}"
}

output "elb_name" {
  value = "${aws_elb.client_name_elb.name}"
}
