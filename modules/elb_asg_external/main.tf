# Variables for ELB
variable "client_name" {}
variable "elb_subnets" {}
variable "vpc_cidr" {}
variable "environment" {}
variable "instance_security_groups" {}
variable "elb_security_groups" {}
variable "server_role" { default = "tiff-cos" }
variable "internal" { default = "false" }
variable "instance_port" {}
variable "instance_protocol" {}
variable "lb_port" {}
# variable "lb_port2" {}
# variable "ssl_certificate_domain" {}
variable "lb_protocol" {}
# variable "lb_protocol2" {}
variable "health_check_healthy_threshold" { default = "5" }
variable "health_check_unhealthy_threshold" { default = "7" }
variable "health_check_target" {}
variable "health_check_interval" {}
variable "health_check_timeout" {}

# Variables for launch configuration
variable "instance_type" { default = "t2.small" }
variable "key_name" {}
variable "ebs_optimized" { default = "false" }
variable "enable_monitoring" { default = "false" }
variable "associate_public_ip" { default = "false" }
variable "root_volume_type" { default = "gp2" }
variable "root_volume_size" { default = "30" }
variable "ami_filter_value" { default = "ubuntu-xenial-16.04-amd64-server-*" }
variable "ami_owner" { default = "099720109477" }
variable "user_data_script" {}
variable "iam_instance_profile" {}

# Variables for autoscaling group
variable "asg_subnets" {}
variable "asg_max_size" {}
variable "asg_min_size" {}
variable "asg_health_check_grace_period" { default = "300" }
variable "asg_health_check_type" { default = "ELB" }
variable "asg_desired_capacity" {}
variable "asg_termination_policy" { default = "OldestInstance"}

# # Find the SSL certificate
# data "aws_acm_certificate" "client_name_ssl_cert" {
#   domain = "${var.ssl_certificate_domain}"
#   statuses = ["ISSUED"]
# }

# Create an external ELB
resource "aws_elb" "client_name_elb" {
  name            = "${var.client_name}-${var.environment}"
  subnets         = ["${split(",", var.elb_subnets)}"]
  security_groups = ["${split(",", var.elb_security_groups)}"]
  internal        = "${var.internal}"

  listener {
    instance_port     = "${var.instance_port}"
    instance_protocol = "${var.instance_protocol}"
    lb_port           = "${var.lb_port}"
    lb_protocol       = "${var.lb_protocol}"
  }

  # listener {
  #   instance_port      = "${var.instance_port}"
  #   instance_protocol  = "${var.instance_protocol}"
  #   lb_port            = "${var.lb_port2}"
  #   lb_protocol        = "${var.lb_protocol2}"
  #   ssl_certificate_id = "${data.aws_acm_certificate.client_name_ssl_cert.arn}"
  # }

  health_check {
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    timeout             = "${var.health_check_timeout}"
    target              = "${var.health_check_target}"
    interval            = "${var.health_check_interval}"
  }
  tags {
    Name        = "${var.client_name}-${var.environment}-elb"
    Role        = "${var.server_role}"
    Environment = "${var.environment}"
    Terraform   = "true"
  }
}

# Find the custom AMI by region
data "aws_ami" "ami_by_region" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ami_filter_value}"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  # Owner of AMI
  owners = ["${var.ami_owner}"]
}

# Setup launch configuration
resource "aws_launch_configuration" "client_name_launch_config" {
  name_prefix                 = "${var.client_name}-${var.environment}-launch-config-"
  image_id                    = "${data.aws_ami.ami_by_region.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  iam_instance_profile        = "${var.iam_instance_profile}"
  security_groups             = ["${split(",", var.instance_security_groups)}"]
  #user_data                   = "${file(var.user_data_script)}"
  user_data                   = "${var.user_data_script}"
  ebs_optimized               = "${var.ebs_optimized}"
  enable_monitoring           = "false"
  associate_public_ip_address = "${var.associate_public_ip}"

  root_block_device {
    volume_type = "${var.root_volume_type}"
    volume_size = "${var.root_volume_size}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Setup scaling group
resource "aws_autoscaling_group" "client_name_autoscaling_group" {
  name                      = "${var.client_name}-${var.environment}-asg"
  max_size                  = "${var.asg_max_size}"
  min_size                  = "${var.asg_min_size}"
  health_check_grace_period = "${var.asg_health_check_grace_period}"
  health_check_type         = "${var.asg_health_check_type}"
  desired_capacity          = "${var.asg_desired_capacity}"
  termination_policies      = ["${var.asg_termination_policy}"]
  launch_configuration      = "${aws_launch_configuration.client_name_launch_config.name}"
  vpc_zone_identifier       = ["${split(",", var.asg_subnets)}"]
  enabled_metrics           = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity", "GroupInServiceInstances", "GroupStandbyInstances", "GroupTerminatingInstances","GroupTotalInstances"]
  metrics_granularity       = "1Minute"
  load_balancers            = ["${aws_elb.client_name_elb.name}"]

  tag {
    key                 = "Name"
    value               = "${var.client_name}-${var.environment}-asg"
    propagate_at_launch = true
  }

  tag {
    key                 = "Terraform"
    value               = "true"
    propagate_at_launch = true
  }
}

# Setup scaling policies
resource "aws_autoscaling_policy" "scale-up-policy" {
  name                   = "${var.client_name}-${var.environment}-scale-up"
  scaling_adjustment     = 2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.client_name_autoscaling_group.name}"
}

resource "aws_cloudwatch_metric_alarm" "scale-up" {
  alarm_name             = "${var.client_name}-${var.environment}-scale-up-alarm"
  comparison_operator    = "GreaterThanOrEqualToThreshold"
  evaluation_periods     = "2"
  metric_name            = "TargetResponseTime"
  namespace              = "AWS/EC2"
  period                 = "60"
  statistic              = "Average"
  threshold              = "1"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.client_name_autoscaling_group.name}"
  }

  alarm_description      = "This metric monitors EC2 TargetResponseTime"
  alarm_actions          = ["${aws_autoscaling_policy.scale-up-policy.arn}"]
}

resource "aws_autoscaling_policy" "scale-down-policy" {
  name                   = "${var.client_name}-${var.environment}-scale-down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.client_name_autoscaling_group.name}"
}

resource "aws_cloudwatch_metric_alarm" "scale-down" {
  alarm_name             = "${var.client_name}-${var.environment}-scale-down-alarm"
  comparison_operator    = "LessThanOrEqualToThreshold"
  evaluation_periods     = "2"
  metric_name            = "CPUUtilization"
  namespace              = "AWS/EC2"
  period                 = "300"
  statistic              = "Average"
  threshold              = "5"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.client_name_autoscaling_group.name}"
  }

  alarm_description      = "This metric monitors EC2 TargetResponseTime"
  alarm_actions          = ["${aws_autoscaling_policy.scale-down-policy.arn}"]
}